package opencart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Base {
    public WebDriver driver;

    public WebDriver initializeDriver(){
        String webriverPath = System.getProperty("user.dir") + "/tools/chromedriver/chromedriver";
        System.setProperty("webdriver.chrome.driver", webriverPath);
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        return driver;
    }
}
