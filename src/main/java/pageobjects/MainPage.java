package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private WebDriver driver;

    private By myAccountBy = By.xpath("//*[@id='top-links']/ul/li[2]/a");
    private By registerBy = By.xpath("//*[text()='Register']");

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getMyAccount() {
        return driver.findElement(myAccountBy);
    }

    public WebElement getRegister() {

        return driver.findElement(registerBy);
    }
}