package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
    private WebDriver driver;

    private By privacyCheckBoxBy = By.name("agree");
    private By continueBtnBy = By.xpath("//*[@id=\'content\']/form/div/div/input[2]");
    private By warningFirstNameBy = By.xpath("//*[@id=\'account\']/div[2]/div/div");
    private By warningLastNameBy = By.xpath("//*[@id=\'account\']/div[3]/div/div");
    private By warningEmailBy = By.xpath("//*[@id=\'account\']/div[4]/div/div");
    private By warningPhoneBy = By.xpath("//*[@id=\'account\']/div[5]/div/div");
    private By warningPasswordBy = By.xpath("//*[@id='content']/form/fieldset[2]/div[1]/div/div");
    private By warningMissingBy = By.xpath("//*[@id=\'account-register\']/div[1]");

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getPrivacyCheckBox() {

        return driver.findElement(privacyCheckBoxBy);
    }

    public WebElement getContinueBtn() {
        return driver.findElement(continueBtnBy);
    }

    public WebElement getWarningFirstName() {
        return driver.findElement(warningFirstNameBy);
    }

    public WebElement getWarningLastName() {

        return driver.findElement(warningLastNameBy);
    }

    public WebElement getWarningEmail() {

        return driver.findElement(warningEmailBy);
    }

    public WebElement getWarningPhone() {

        return driver.findElement(warningPhoneBy);
    }

    public WebElement getWarningPassword() {

        return driver.findElement(warningPasswordBy);
    }

    public WebElement getwarningMissing() {
        return driver.findElement(warningMissingBy);
    }
}
