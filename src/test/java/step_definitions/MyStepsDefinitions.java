package step_definitions;

import com.google.common.base.Verify;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import opencart.Base;
import org.junit.runner.RunWith;
import pageobjects.MainPage;
import pageobjects.SignUpPage;

@RunWith(Cucumber.class)
public class MyStepsDefinitions extends Base {
    SignUpPage signUpPage;
    MainPage mainPage;

    @Given("^Initialize chromedriver$")
    public void intialize_chromedriver() {
        driver = initializeDriver();
        signUpPage = new SignUpPage(driver);
        mainPage = new MainPage(driver);
    }

    @And("^User navigates to the url \"([^\"]*)\"$")
    public void user_navigates_to_the_url_something(String strArg1){
        driver.get(strArg1);
    }

    @Given("^User does not fill any field$")
    public void user_does_not_fill_any_field(){

    }

    @When("^User Submits$")
    public void user_submits(){
        signUpPage.getPrivacyCheckBox().click();
        signUpPage.getContinueBtn().click();
    }

    @Then("^Error message detailing what mandatory fields are missing$")
    public void error_message_detailing_what_mandatory_fields_are_missing() {
        Verify.verify(signUpPage.getWarningFirstName().isDisplayed());
        Verify.verify(signUpPage.getWarningLastName().isDisplayed());
        Verify.verify(signUpPage.getWarningEmail().isDisplayed());
        Verify.verify(signUpPage.getWarningPassword().isDisplayed());
        Verify.verify(signUpPage.getWarningPhone().isDisplayed());
        Verify.verify(signUpPage.getwarningMissing().isDisplayed());
        driver.close();
    }


    @And("^selects Sign Up$")
    public void selects_sign_up() {
        mainPage.getMyAccount().click();
        mainPage.getRegister().click();
    }

}

